$(function () {
    
    /**
     * DELETE FUNCTION
     * */

    $('.btn-apply').on('click', function () {
        if($('.sel-inp').val() == 'delete'){
            var arr = [];
            $('.product').each(function () {
                var inp = $(this).find('input');
                if(inp.attr('checked')){
                    var sku = $(this).find('i').html();
                    arr.push(sku);
                }
            });

            if(arr.length > 0){
                $.ajax({
                    url: 'inc/function.php',
                    data: {'action' : 'delete', 'array': JSON.stringify(arr)},
                    type: 'POST',
                    success: function(data){
                        if(data == 'ok'){
                            $('.product').each(function () {
                                var inp = $(this).find('input');
                                if(inp.attr('checked')){
                                    $(this).hide();
                                }
                            });
                        }
                    }
                });
            }
        }
    });
    /**
    * SELECT PRODUCT ON CLICK
    * */

   $(document).on('click', '.product', function () {
       var inp = $(this).find('input');
       if(!inp.attr('checked')){
           inp.attr('checked', true);
       }else{
           inp.attr('checked', false);
       }
   });

    /**
    * ADD PRODUCT TO DB
    * */
    function getImageUrl(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.bg-img').css("background-image", "url(" + e.target.result + ")");
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#item_price').on('input', function() {
        this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
    });

    $("#item_file").change(function(){
        getImageUrl(this);
    });
    $('#item_name').on('change', function () {
       $('.product-example h4').html($(this).val());
    });
    $('#item_price').on('change', function () {
        $('.product-example h5').html('<span>$</span>'+$(this).val());
    });
    $('#item_sku').on('change', function () {
        $('.product-example i').html($(this).val());
    });

    $('.save-btn').on('click', function () {

        var item_price = $('#item_price').val();
        var item_name = $('#item_name').val();
        var item_sku = $('#item_sku').val();

        var form = new FormData();
        form.append( 'item_file', $('#item_file')[0].files[0] );
        form.append( 'save', 'save' );
        form.append( 'item_price', item_price);
        form.append( 'item_name', item_name);
        form.append( 'item_sku', item_sku);
        
        if((item_price != '') && (item_name != '') && (item_sku != '')){
            $.ajax({
                url: 'inc/function.php',
                data: form,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(data){
                    if(data == 'ok'){
                        $('#item_price').val('');
                        $('#item_name').val('');
                        $('#item_sku').val('');
                        $("#item_file").val('');
                        $('.error_block').html('Success!').css('color', 'green').show();
                        setTimeout(function () {
                           $('.error_block').hide();
                        }, 3000);
                    }else{
                        $('.error_block').html(data).css('color', 'red');
                    }
                }
            });
        }else{
            $('.error_block').html('All fields must be filled').css('color', 'red');
        }

        
    });

});