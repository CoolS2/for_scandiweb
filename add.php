<html>
<head>
    <meta charset="UTF-8">
    <title>Product list</title>
    <link rel="stylesheet" type="text/css" href="main.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>

<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand" href="#">For Scandiweb</a>

    <div class="collapse navbar-collapse" >
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.php">Product list</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="add.php">Add products</a>
            </li>

        </ul>
    </div>
</nav>

<div class="container">
    <header>
        <div class="row">
            <div class="col-md-6">
                <h3>Add product</h3>
            </div>
            <div class="col-md-6 select-block">
                <form action="javascript:" method="POST">
                    <button type="submit" class="btn btn-primary save-btn">Save</button>
                </form>
            </div>
        </div>
    </header>
    <section class="content add-product-block">
        <div class="row">
            <div class="col-md-6">
                <form action="javascript:" method="POST">
                    <input type="file" id="item_file" name="item_file">
                    <input type="text" class="form-control" placeholder="Name" id="item_name" name="item_name">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input type="text" class="form-control" placeholder="Price" id="item_price" name="item_price">
                        <div class="input-group-addon">$</div>
                    </div>
                    <input type="text" class="form-control" placeholder="SKU" id="item_sku" name="item_sku">
                </form>
                <p class="error_block"></p>
            </div>
            <div class="col-md-6">
                <div class="product-example product">
                    <div title="Item image" class="bg-img" style="background-image: url(images/no-image.png)"></div>
                    <h4 title="Item name"></h4>
                    <h5 title="Item price"></h5>
                    <i title="Unique number"></i>
                </div>
            </div>
        </div>
    </section>
</div>
</body>
<script
    src="https://code.jquery.com/jquery-2.2.4.min.js"
    integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
    crossorigin="anonymous"></script>
<script src="js/main.js"></script>
</html>