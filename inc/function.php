<?php


/**
 * ADD NEW ITEM
 * */

if(isset($_POST['save'])){
    include_once('db.php');

    $item_name = htmlspecialchars(trim($_POST['item_name']));
    $item_file = htmlspecialchars(trim($_POST['item_file']));
    $item_price = htmlspecialchars(trim($_POST['item_price']));
    $item_sku = htmlspecialchars(trim($_POST['item_sku']));


    $obj = new DB();
    $sku = $obj->select($item_sku);
        if(!$obj->error){
            if(!$sku){ // Check if sku is unique

                /**
                 * CHECK FILE
                 * */

                if(isset($_FILES['item_file'])){

                    $obj->uploadFile("item_file"); // Check file and upload
                    if($obj->file_status){ // If status is uploaded
                        $obj->insert($obj->filename,$item_name,$item_price,$item_sku);
                        exit('ok');
                    }else{
                        exit('Problem with file');
                    }

                }else{
                    exit('Image required!');
                }
            }else{
                exit("Item already exist");
            }
        }else{
            exit($obj->error); // if mysql error
        }


}



if(isset($_POST['action'])){

    /**
     * ADD THE PRODUCT TO CONTENT
     * */

    if($_POST['action'] == 'action'){
        include_once('db.php');

        $obj = new DB();
        $select = $obj->loadContent();
        if(!$obj->error){
            if($select){
                $data = '';
                foreach($select as $key => $value){
                    $data .= '<div class="col-md-3">
                        <div class="product">
                            <input type="checkbox">
                            <div class="bg-img" style="background-image: url(images/upload/'.$value['photo'].')"></div>
                            <h4>'.$value['name'].'</h4>
                            <h5><span>$</span>'.$value['price'].'</h5>
                            <i>'.$value['sku'].'</i>
                        </div>
                    </div>';
                }
                exit($data);
            }else{
                exit('Empty');
            }
        }else{
            exit($obj->error); // if mysql error
        }

    }

    /**
     * DELETE SELECTED ITEMS
     * */

    if($_POST['action'] == 'delete'){
        include_once('db.php');
        $arr = $_POST['array'];
        $arr = json_decode($arr);
        
        $obj = new DB();
        $obj->deleteContent($arr);
        if(!$obj->error){
            exit('ok');
        }
        exit('error');
    }
}