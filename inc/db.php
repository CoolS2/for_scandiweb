<?php

class DB{

    // FOR mysql
    private $servername = "localhost";
    private $username = "cools2_skola";
    private $password = "Aleksandr1";
    private $dbname = "cools2_skola";
    public $sql;
    public $error;

    //For file
    private $src = "../images/upload/";
    public $tmp;
    public $filename;
    private $type;
    public $uploadfile;
    public $file_status;
    private $allowed = array('png' ,'jpg');
    private $ext;

    function __construct(){
        try{
            $this->sql = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
            $this->sql->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException  $e ){
            $this->error = "Error: ".$e;
        }
    }
    public function insert($photo, $name, $price, $sku){
        $sql = 'INSERT INTO test_table (photo,name,price,sku) VALUES("'.$photo.'","'.$name.'","'.$price.'","'.$sku.'")';
        $query = $this->sql->prepare($sql);
        $query->execute();
    }

    public function select($sku){

            $sql = 'SELECT sku FROM test_table WHERE sku = ?';
            $query = $this->sql->prepare($sql);
            $query->execute(array($sku));
            $result = $query->fetchColumn();
            return $result;

    }

    public function uploadFile($string){
        $this -> filename = str_replace(' ', '_', $_FILES[$string]["name"]); //check name, if spaces, replace to _
        $this -> tmp = $_FILES[$string]["tmp_name"];
        $this -> type = $_FILES[$string]['type'];
        $this -> uploadfile = $this -> src . basename($this -> filename);

        $this -> ext = pathinfo($this -> filename, PATHINFO_EXTENSION); // Check file extension
        if(!in_array($this->ext,$this->allowed) ) { // If file extension is not allowed
            $this->file_status = false;
        }else{
            if(move_uploaded_file($this -> tmp, $this -> uploadfile)){ // If file extension allowed, upload
                $this->file_status = true;
            }else{
                $this->file_status = false;
            }

        }
        return;
    }


    public function loadContent(){
            $sql = 'SELECT * FROM test_table';
            $query = $this->sql->query($sql);
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
            return $result;
    }

    public function deleteContent($arr){

        $in  = str_repeat('?,', count($arr) - 1) . '?';
        $sql = 'DELETE FROM test_table WHERE sku IN ('.$in.')';
        $query = $this->sql->prepare($sql);
        $query->execute($arr);

    }

    function __destruct() {
        $this->sql = null;
    }

}