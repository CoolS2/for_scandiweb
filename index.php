<html>
    <head>
        <meta charset="UTF-8">
        <title>Product list</title>
        <link rel="stylesheet" type="text/css" href="main.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    </head>

    <body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <a class="navbar-brand" href="#">For Scandiweb</a>

        <div class="collapse navbar-collapse" >
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Product list</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="add.php">Add products</a>
                </li>

            </ul>
        </div>
    </nav>

        <div class="container">
            <header>
                <div class="row">
                    <div class="col-md-6">
                        <h3>Product list</h3>
                    </div>
                    <div class="col-md-6 select-block">
                        <form action="javascript:" method="POST">

                            <select class="custom-select sel-inp mb-2 mr-sm-2 mb-sm-0">
                                <option selected>Choose...</option>
                                <option value="delete">Mass Delete Action</option>
                            </select>

                            <button type="submit" class="btn btn-primary btn-apply">Apply</button>
                        </form>
                    </div>
                </div>
            </header>
            <section class="content">
                <div class="row">
                    <div class="col-md-3">
                        <div class="product">
                            <input type="checkbox">
                            <div class="bg-img" style="background-image: url(images/acme.png)"></div>
                            <h4>Acme disk</h4>
                            <h5><span>$</span>10.00</h5>
                            <i>JVC200142</i>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product">
                            <input type="checkbox">
                            <div class="bg-img" style="background-image: url(images/acme.png)"></div>
                            <h4>Acme disk</h4>
                            <h5><span>$</span>10.00</h5>
                            <i>JVC200142</i>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product">
                            <input type="checkbox">
                            <div class="bg-img" style="background-image: url(images/acme.png)"></div>
                            <h4>Acme disk</h4>
                            <h5><span>$</span>10.00</h5>
                            <i>JVC200142</i>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product">
                            <input type="checkbox">
                            <div class="bg-img" style="background-image: url(images/acme.png)"></div>
                            <h4>Acme disk</h4>
                            <h5><span>$</span>10.00</h5>
                            <i>JVC200142</i>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product">
                            <input type="checkbox">
                            <div class="bg-img" style="background-image: url(images/acme.png)"></div>
                            <h4>Acme disk</h4>
                            <h5><span>$</span>10.00</h5>
                            <i>JVC200142</i>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product">
                                <input type="checkbox">
                            <div class="bg-img" style="background-image: url(images/acme.png)"></div>
                            <h4>Acme disk</h4>
                            <h5><span>$</span>10.00</h5>
                            <i>JVC200142</i>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </body>
    <script
        src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>
        <script src="js/main.js"></script>
    <script>
        $(function () {
            /**
             * LOAD THE PRODUCT TO CONTENT
             * */

                $.ajax({
                    url: 'inc/function.php',
                    data: {'action' : 'action'},
                    dataType: "html",
                    type: 'POST',
                    beforeSend: function() {$('.content .row').html('Loading...');},
                    success: function(data){
                        $('.content .row').html(data);
                    }
                });

        });
    </script>
</html>